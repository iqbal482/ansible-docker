const express = require('express')
const router = express.Router();
const http = require('http')
const bodyParser = require('body-parser');
const logger = require('morgan');
const Sequelize = require('sequelize')
const jwt = require('jsonwebtoken');
const rendom = require('rendom');
const authroutes = require('./routes/auth');
const toolroutes = require('./routes/tool');
const paymentroutes = require('./routes/payment');
const indexroutes = require('./routes');
const nodemailer = require('nodemailer')
const utils = require('./utils.js')
const Middleware = require('./helpers/middlewares')

const cors = require('cors')
const env = process.env.NODE_ENV || 'localhost'
const dbconfig = require('./configs/db.config.json')[env]
console.log(env)
const appconfig = require('./configs/app.config.json')
const emailconfig = appconfig.emailcredential[env]
const app = express()

app.use(bodyParser.json());
app.use(logger('dev'));
app.use(cors());

utils.connectAMQP(appconfig.rbmqsetting[env].rbmqexchange)
// ================ DATABASE
const sequelize = new Sequelize(dbconfig.database, dbconfig.username, dbconfig.password, dbconfig)

// ================ SCHEMA
const userSchema = sequelize.define('users', {
    id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
    },
    fullname:{
        type: Sequelize.STRING,
        allowNull: false
    },
    email:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password:{
        type: Sequelize.STRING,
        allowNull: false
    },
    gender:{
        type: Sequelize.STRING,
    },
    occupation:{
        type: Sequelize.STRING,
    },
    phone:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    birthday: Sequelize.STRING,
    address: Sequelize.STRING,
    postalcode: Sequelize.STRING,
    devicetoken: Sequelize.STRING,
    walletcode:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        defaultValue: ''
    },
    city:{
        type: Sequelize.STRING
    },
    country:{
        type: Sequelize.STRING
    }
})

const citySchema = sequelize.define('city', {
    id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
    },
    name:{
        type: Sequelize.STRING,
        allowNull: false
    }
})

const otpSchema = sequelize.define('otp', {
    id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
    },
    phone: {
        type: Sequelize.STRING,
        allowNull: false
    },
    otp: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
})

const payments_methodSchema = sequelize.define('payments_method', {
    id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
    },
    userid: {
        type: Sequelize.UUID
    },
    UserNumber:{
        type: Sequelize.STRING,
        allowNull: false
    },
    PassCvc:{
        type: Sequelize.STRING,
        allowNull: false
    },
    ExpiredDate:{
        type: Sequelize.STRING,
        allowNull: true
    },
    Type:{
        type: Sequelize.ENUM,
        values: ['visa', 'mastercard', 'dana','doku'],
        allowNull: false
    }
})

const loginSchema = sequelize.define('login', {
    id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
    },
    loginAt:{
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW,
    }
})

// ================ END SCHEMA

// ================ ASSOCIATION
loginSchema.belongsTo(userSchema)
// ================ END OF ASSOCIATION


const transporter = nodemailer.createTransport({
    service: 'gmail',
    host: emailconfig.email_smtp,
    //port: config.email_smtp_port,
    //secureConnection: false,
    //requiresAuth: true,
    //secure: true, // use SSL
    auth: {
        user: emailconfig.email_user,
        pass: emailconfig.email_password
    }
})


app.use((req, res, next) => {
    req.db = sequelize
    req.users = userSchema
    req.otp = otpSchema
    req.city = citySchema
    req.payments_method = payments_methodSchema
    req.login = loginSchema
    req.transporter = transporter
    next()
})
app.use(appconfig.middleURL+'/', indexroutes);
app.use(appconfig.middleURL+'/auth', authroutes);
app.use(appconfig.middleURL+'/tool', toolroutes);
app.use(appconfig.middleURL+'/payment', paymentroutes);
const server = http.createServer(app);

sequelize.sync().then(function () {
    server.listen(appconfig.port, function(){
        var dburl =  dbconfig.dialect+'://'+dbconfig.username+'@'+dbconfig.host+'/'+dbconfig.database
        console.log('[DB] connected', dburl)
        console.log('[USER-SERVICE] service listening on port', appconfig.port)
    })
});
