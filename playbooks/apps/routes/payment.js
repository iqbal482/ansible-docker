const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const fs = require('fs')
const env = process.env.NODE_ENV || 'localhost'
const appconfig = require('../configs/app.config.json')
const bcrypt = require('bcrypt')
const request = require('request')
const rendom = require('rendom')
const utils = require('../utils.js')

// hash
function generateHash(password) {
  return bcrypt.hashSync(password, 10)
};

// checking if password is valid
function isValidPassword(password, hash) {
    return bcrypt.compareSync(password, hash)
};

// Middleware for JWT authentication
function verifytoken(checkIsAdmin = false) {
    return (req, res, next) => {
        let token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ','')
        }
        jwt.verify(token, appconfig.secretkey, (err, decoded) => {
            if (err) {
                res.sendStatus(403)
            } else {
                if (checkIsAdmin == true) {
                    if (decoded.isAdmin == true) {
                        req.token = decoded
                        next()
                    } else {
                        res.sendStatus(403)
                    }
                } else {
                    req.token = decoded
                    next()
                }
            }
        })
    }
}

router.post('/data', verifytoken(), (req, res) => {
    req.payments_method.findOne({
        where: {
                UserNumber: req.body.UserNumber
            }
    }).then((response) => {
        if (response) {
            res.status(500).json({'error': 'Data already exists'})
        } else {
            req.body.userid = req.token.userid
            req.payments_method.create(req.body)
            .then((resultcreate) => {
                res.json(resultcreate)
            })
        }
    })
})

router.get('/data', verifytoken(), (req, res) => {
    req.payments_method.findAll({
        where: {
            userid: req.token.userid
        }
    })
    .then((response) => {
        if (!response) {
            res.status(404).end('data not found')
        } else {
            res.json(response)
        }
    }).catch((error) => {
        res.sendStatus(500, error.message)
    })
})

router.get('/data/:id', verifytoken(), (req, res) => {
    console.log(req.params.id)
    req.payments_method.findAll({
        where: {
            userid: req.token.userid,
            id:req.params.id
        }
    })
    .then((response) => {
        if (!response) {
            res.status(404).end('data not found')
        } else {
            res.json(response)
        }
    }).catch((error) => {
        res.sendStatus(500, error.message)
    })
})

router.put('/data', verifytoken(), (req, res) => {
    // drop immutable field

    req.payments_method.update(req.body,{ where: { userid: req.token.userid,id:req.body.id }, returning: true })
    .then((response) => {
        res.json(response[1][0])
    }).catch((error) => {
        res.sendStatus(500, error)
    })
})

router.delete('/data', verifytoken(), (req, res) => {
    req.payments_method.destroy({
        where: {
            userid: req.token.userid,
            id:req.body.id
        }
    })
    .then((response) => {
        if (!response) {
            res.status(404).end('data not found')
        } else {
            req.payments_method.destroy({
                where: {
                    userid: req.token.userid,
                    id:req.body.id
                }
            }).then((response) => {
                res.json(response)
            })
        }
    }).catch((error) => {
        res.sendStatus(500, error.message)
    })
})







module.exports = router
