const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const fs = require('fs')
const bcrypt = require('bcrypt')
const crypto = require('crypto')
const request = require('request')
const rendom = require('rendom')
const utils = require('../utils.js')
const queryString = require("querystring");
const env = process.env.NODE_ENV || 'localhost'
const appconfig = require('../configs/app.config.json')
const emailconfig = appconfig.emailcredential[env]

String.prototype.insert = function (index, string) {
  if (index > 0)
    return this.substring(0, index) + string + this.substring(index, this.length);
  else
    return string + this;
};

// hash
function generateHash(password) {
  return bcrypt.hashSync(password, 10)
};

// checking if password is valid
function isValidPassword(password, hash) {
    return bcrypt.compareSync(password, hash)
};

// Middleware for JWT authentication
function verifytoken(checkIsAdmin = false) {
    return (req, res, next) => {
        let token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ','')
        }
        jwt.verify(token, appconfig.secretkey, (err, decoded) => {
            if (err) {
                res.sendStatus(403)
            } else {
                if (checkIsAdmin == true) {
                    if (decoded.isAdmin == true) {
                        req.token = decoded
                        next()
                    } else {
                        res.sendStatus(403)
                    }
                } else {
                    req.token = decoded
                    next()
                }
            }
        })
    }
}

router.get('/', (req, res) => {
    res.send('auth')
})

router.get('/allusers', verifytoken(true), (req, res) => {
    req.users.findAll()
    .then((response) => {
        res.json(response)
    }).catch((error) => {
        res.sendStatus(500, error.message)
    })
})

// create
router.post('/register', (req, res) => {
    let pwd = generateHash(req.body.password)
    req.body.password = pwd
    req.body.walletcode = rendom.generateNumber(10)

    // default value
    if (req.body.city === undefined || req.body.city === null || req.body.city === '') {
        req.body.city = 'Others'
    }
    if (req.body.occupation === undefined || req.body.occupation === null || req.body.occupation === '') {
        req.body.occupation = 'Others'
    }

    req.users.findOne({
        where: {
            $or: [
                {email: req.body.email},
                {phone: req.body.phone}
            ]
        }
    }).then((response) => {
        if (response) {
            res.status(500).json({'error': 'email or phone already exists'})
        } else {
            req.users.create(req.body)
            .then((resultcreate) => {
                res.json(resultcreate)
            })
        }
    })

})

router.post('/login', (req, res) => {

    if (!req.body.phone || !req.body.password) {
        return res.status(500).end('Login data incomplete')
    }
    let cleanphone = req.body.phone
    let noplusphone = cleanphone.replace(/\D/g, '')
    cleanphone = cleanphone.slice(2 - noplusphone.length).insert(0,"0")
    req.users.findOne({
        where: {
            $or: [{phone: cleanphone}, {phone: req.body.phone}, {phone: noplusphone}]
        },
        //include: [req.city]
    }).then((response) => {
        if (!response) {
            return res.status(404).json({'error': 'User not found'})
        }

        // compare password
        if (isValidPassword(req.body.password, response.password) == false) {
            return res.status(404).json({'error': 'User not found'})
        }

        // create JWT token
        const user = {
            userid: response.id,
            phone: response.phone,
            email: response.email,
        }
        const options = { expiresIn: appconfig.ttltoken };
        const token = jwt.sign(user, appconfig.secretkey, options);

        // create OTP
        let retval = {
            token: token,
            userdata: response.toJSON()
        }


        // update device token
        if (req.body.devicetoken) {
            response.devicetoken = req.body.devicetoken
            response.save().then((result) => {
                console.log('[UPDATE DEVICE TOKEN] Successfully update device token for user '+response.username+' : '+req.body.devicetoken)
                retval.userdata = result
                res.json(retval)
            })
        } else {
            res.json(retval)
        }

        // create login data
        let logindata = {
            userId: response.id,
            loginAt: new Date()
        }
        req.login.create(logindata).then((result) => {})

    })
})

router.post('/logout', verifytoken(), (req, res) => {
    req.users.findOne({
        where: {
            id: req.token.userid
        }
    }).then((response) => {
        if (!response) {
            return res.status(404).json({'error': 'User not found'})
        } else {
            response.devicetoken = ''
            response.save().then((result) => {
                console.log('[REMOVE DEVICE TOKEN] Successfully remove device token for user '+response.username);
                res.json(true)
            })
        }
    })
})

router.post('/readusers', verifytoken(true), (req, res) => {
    console.log(req.body)
    req.users.findAll({
        where: {
            id: req.body.users
        },
        //include: [req.city]
        //attributes: ['id', 'username','fullname','devicetoken']
    })
    .then((response) => {
        res.json(response)
    }).catch((error) => {
        res.sendStatus(500, error.message)
    })
})


router.get('/userbyid/:userid', verifytoken(true), (req, res) => {
    req.users.findOne({
        where: {
            id: req.params.userid
        }
    })
    .then((response) => {
        if (!response) {
            res.status(404).end('user not found')
        } else {
            res.json(response)
        }
    }).catch((error) => {
        res.sendStatus(500, error.message)
    })
})

// read my profile
router.get('/myprofile', verifytoken(), (req, res) => {
    req.users.findOne({
        where: {
            phone: req.token.phone
        }
    })
    .then((response) => {
        if (response == null) {
            res.sendStatus(404)
        } else {
            res.json(response)
        }
    }).catch((error) => {
        res.sendStatus(500, error)
    })
})


// update
router.put('/update', verifytoken(), (req, res) => {
    // drop immutable field
    delete req.body.phone
    delete req.body.email
    delete req.body.password

    req.users.update(req.body,{ where: { phone: req.token.phone }, returning: true })
    .then((response) => {
        res.json(response[1][0])
    }).catch((error) => {
        res.sendStatus(500, error)
    })
})

router.put('/forgotpasswordold', (req, res) => {
    if (!req.body.email) {
        res.status(500).end('email cannot be empty')
    }
    req.users.findOne({
        where: {
            email: req.body.email,
        }
    }).then((response) => {
        if (!response) {
            res.status(404).end('user not found')
        } else {

            let newpwd = rendom.generateNumberAndString(6)
            let pwd = generateHash(newpwd)
            response.password = pwd
            response.save()
            .then((response) => {
                let options = {
                    from: emailconfig.email_user,
                    to: req.body.email,
                    subject: 'Meeberian - Forgot Password',
                    html: '<p>Hello <b>'+response.fullname+'</b></p><br/><p>Your new password is <b>'+newpwd+'</b></p>'
                }
                req.transporter.sendMail(options, (error, info) => {
                    if(error){
                        console.log('[EMAIL] Failed send email to '+req.body.email)
                        console.log(error)
                    } else {
                        console.log('[EMAIL] Successfully send email to '+req.body.email)
                        console.log(info.response)
                    }
                })
                res.json(response)
            })
        }
    })
})

router.put('/forgotpassword', (req, res) => {
    if (!req.body.email) {
        res.status(500).end('email cannot be empty')
    }
    req.users.findOne({
        where: {
            email: req.body.email,
        }
    }).then((response) => {
        if (!response) {
            res.status(404).end('user not found')
        } else {

            let json = {
                userId: response.id,
                date: new Date().getTime()
            }
            let fptoken = utils.encryptAES(JSON.stringify(json))
            let url = appconfig.updatepasswordlandingpage+fptoken

            let options = {
                from: emailconfig.email_user,
                to: req.body.email,
                subject: 'Meeberian - Forgot Password',
                html: '<p>Hello <b>'+response.fullname+'</b></p><br/><p>Click <a href="'+url+'">here</a> to change your password</p>'
            }
            req.transporter.sendMail(options, (error, info) => {
                if(error){
                    console.log('[EMAIL] Failed send email to '+req.body.email)
                    console.log(error)
                } else {
                    console.log('[EMAIL] Successfully send email to '+req.body.email)
                    console.log(info.response)
                }
            })
            res.json({
                id: response.id,
                fullname: response.fullname
            })
        }
    })
})

router.put('/updatepassword', (req, res, next) => {
    if (!req.body.password || !req.body.token) {
        res.status(500).end('Invalid request')
    }

    try {
        let decoded = utils.decryptAES(req.body.token)
        let json = JSON.parse(decoded)
        // ttl
        let tokenDate = json.date
        let currentDate = new Date().getTime()
        let hours = Math.abs(currentDate - tokenDate) / 3600000
        if (hours > 1) {
            res.status(500).end('Expired token')
            return next()
        }
        req.users.findOne({
            where: {
                id: json.userId
            }
        }).then((response) => {
            if (!response) {
                res.status(404).end('user not found')
            } else {
                let pwd = generateHash(req.body.password)
                response.password = pwd
                response.save()
                .then((response) => {
                    res.status(200).json({message: 'password updated'})
                })
            }
        })
    } catch(error) {
        res.status(500).end(error.message)
    }
})

router.post('/changepassword', verifytoken(), (req, res) => {
    if (!req.body.password) {
        res.status(500).end('password cannot be empty')
    }

    req.users.findOne({
        where: {
            id: req.token.userid
        }
    }).then((response) => {
        if (!response) {
            res.status(404).end('user not found')
        } else {

            let pwd = generateHash(req.body.password)
            response.password = pwd

            response.save()
            .then((response) => {
                if (!response) {
                    res.status(500).end('error on saving password')
                } else {
                    res.json(true)
                }
            })
        }
    })
})

router.post('/requestotp', (req, res) => {
    if (!req.body.phone) {
        return res.sendStatus(500)
    }

    let cleanphone = req.body.phone
    let noplusphone = cleanphone.replace(/\D/g, '')
    cleanphone = cleanphone.slice(2 - noplusphone.length).insert(0,"0")

    req.users.find({
        where: {
            $or: [{phone: cleanphone}, {phone: req.body.phone}, {phone: noplusphone}]
        }
    }).then((response) => {
        console.log(response)
        if (response == null) {
            let otp = Math.floor(Math.random()*90000) + 10000

            let smsbody = 'Hello Meeberian folks! Your OTP is '+otp
            const stringified = queryString.stringify({message: smsbody});
            let jatisurl = 'https://sms-api.jatismobile.com/index.ashx?userid=gaharu&password=gaharu235&msisdn='+ req.body.phone +'&'+ stringified +'&sender=info&division=Marketing&batchname=test&uploadby=sandhy&channel=2'
            console.log(jatisurl)
            request(jatisurl, (err, result, body) => {
                if (!err) {
                    console.log('[SEND OTP SMS] successfully send sms to: '+req.body.phone+' with content '+smsbody+' | result: '+body)
                } else {
                    console.log('[SEND OTP SMS] error has occured '+err)
                }
            })

            // save to database
            req.otp.findOne({
                where: {
                    phone: req.body.phone
                }
            }).then((otpdata) => {
                if (!otpdata) {
                    let data = {
                        otp: otp,
                        phone: req.body.phone
                    }
                    req.otp.create(data).then((result) => {
                        res.json({status: true, message: 'OTP on processing'})
                    })
                } else {
                    otpdata.otp = otp
                    otpdata.save().then((result) => {
                        res.json({status: true, message: 'OTP on processing'})
                    })
                }
            })
        } else {
            res.status(404).end('user already exists')
        }
    })
})

router.post('/verifyotp', (req, res) => {
    if (!req.body.phone || !req.body.otp) {
        return res.sendStatus(500)
    }
    req.otp.findOne({
        where: {
            otp: req.body.otp,
            phone: req.body.phone
        }
    }).then((result) => {
        if (!result) {
            res.status(404).end('Invalid OTP')
        } else {
            result.destroy()
            res.json(true)
        }
    }).catch((error) => {
        res.status(500).end(error.message)
    })
})

router.get('/userbywalletcode/:walletcode', verifytoken(true), (req, res) => {
    req.users.findOne({
        where: {
            walletcode: req.params.walletcode
        }
    }).then((response) => {
        res.json(response)
    })
})

router.get('/cities', (req, res) => {
    req.city.findAll({
        attributes: ['id', 'name']
    }).then((response) => {
        res.json(response)
    })
})
module.exports = router
