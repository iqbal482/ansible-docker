const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const fs = require('fs')
const env = process.env.NODE_ENV || 'localhost'
const appconfig = require('../configs/app.config.json')
const bcrypt = require('bcrypt')
const request = require('request')
const rendom = require('rendom')
const utils = require('../utils.js')

// Middleware for JWT authentication
function verifytoken(checkIsAdmin = false) {
    return (req, res, next) => {
        let token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ','')
        }
        jwt.verify(token, appconfig.secretkey, (err, decoded) => {
            if (err) {
                res.sendStatus(403)
            } else {
                if (checkIsAdmin == true) {
                    if (decoded.isAdmin == true) {
                        req.token = decoded
                        next()
                    } else {
                        res.sendStatus(403)
                    }
                } else {
                    req.token = decoded
                    next()
                }
            }
        })
    }
}

router.get('/', (req, res) => {
    res.send('tool')
})

module.exports = router
