const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const fs = require('fs')
const env = process.env.NODE_ENV || 'localhost'
const appconfig = require('../configs/app.config.json')
const rbmqconfig = appconfig.rbmqsetting[env]
const bcrypt = require('bcrypt')
const request = require('request')
const rendom = require('rendom')
const utils = require('../utils.js')

// hash
function generateHash(password) {
  return bcrypt.hashSync(password, 10)
};

// checking if password is valid
function isValidPassword(password, hash) {
    return bcrypt.compareSync(password, hash)
};

// Middleware for JWT authentication
function verifytoken(checkIsAdmin = false) {
    return (req, res, next) => {
        let token = req.headers['authorization'];
        if (token) {
            token = token.replace('Bearer ','')
        }
        jwt.verify(token, appconfig.secretkey, (err, decoded) => {
            if (err) {
                res.sendStatus(403)
            } else {
                if (checkIsAdmin == true) {
                    if (decoded.isAdmin == true) {
                        req.token = decoded
                        next()
                    } else {
                        res.sendStatus(403)
                    }
                } else {
                    req.token = decoded
                    next()
                }
            }
        })
    }
}

router.get('/', (req, res) => {
    let where = {}
    if (req.query.phone) {
        let cleanphone = req.query.phone
        let noplusphone = cleanphone.replace(/\D/g, '')
        cleanphone = cleanphone.slice(2 - noplusphone.length).insert(0,"0")
        where = {
            $or: [{phone: cleanphone}, {phone: req.body.phone}, {phone: noplusphone}]
        }
    }
    req.users.findAll({ where: where })
    .then((response) => {
        res.json(response)
    }).catch((error) => {
        res.sendStatus(500, error.message)
    })
})

router.get('/:userid', verifytoken(true), (req, res) => {
    console.log(req.params.userid)
    req.users.findOne({
        where: {
            id: req.params.userid
        }
    })
    .then((response) => {
        if (!response) {
            res.status(404).end('user not found')
        } else {
            res.json(response)
        }
    }).catch((error) => {
        res.sendStatus(500, error.message)
    })
})

router.post('/:userid/callcustomer', verifytoken(true), (req, res) => {
    req.users.findOne({
        where: {
            id: req.params.userid
        }
    }).then((userdata) => {
        if (!userdata) {
            res.status(404).end('user not found')
        } else {
            if (!userdata.devicetoken) {
                res.status(500).end('user has no devicetoken registered')
            } else {
                // ==============================================
                // SEND PUSH NOTIFICATION
                let genericmsg = {
                    message: req.body.message
                }
                let pndata = {
                    token: userdata.devicetoken,
                    front: {
                        title: 'Meeberian',
                        string: (req.body.message == undefined || req.body.message == '') ? '' : req.body.message
                    },
                    message: {
                        type: 'call_customer',
                        body: genericmsg
                    },
                    type: 'pushnotification'
                }
                console.log('[USER -> PROCESSOR] Send push notification for user: '+userdata.fullname)
                utils.doPublishToAMQPExchange(rbmqconfig.rbmqexchange, pndata)
                res.json({"status": "ok"})
                // ==============================================
            }
        }
    })
})



module.exports = router
