const middleware = {
    // Middleware for JWT authentication
    verifytoken: function (checkIsAdmin = false) {
        return (req, res, next) => {
            let token = req.headers['authorization'];
            if (token) {
                token = token.replace('Bearer ','')
            }
            jwt.verify(token, config.secretkey, (err, decoded) => {
                if (err) {
                    res.sendStatus(403)
                } else {
                    if (checkIsAdmin == true) {
                        if (decoded.isAdmin == true) {
                            req.token = decoded
                            next()
                        } else {
                            res.sendStatus(403)
                        }
                    } else {
                        req.token = decoded
                        next()
                    }
                }
            })
        }
    }
}
module.exports = middleware